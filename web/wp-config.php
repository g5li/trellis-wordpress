<?php
/**
 * autoload for env functions
 */
require_once(dirname(__DIR__) . '/vendor/autoload.php');

/**
 * Bootstrap WordPress
 */
if (!defined('ABSPATH')) {
    define('ABSPATH', __DIR__);
}

/** Development */
define('SAVEQUERIES', true);
define('WP_DEBUG', true);
define('SCRIPT_DEBUG', true);

/**
 * This file was copied from https://github.com/roots/bedrock/blob/master/config/application.php,
 * And remove some constant
 */
require __DIR__ . '/wp-config-bedrock.php';

/**
 * Include wp-settings.php
 */
/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
